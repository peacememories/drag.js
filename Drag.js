'use strict'

/*

Name: Drag.js
Author: Gabriel Pickl
Version: 0.0.5
Date: 09.12.2011

Adds events to javascript:

### dragStart ###

event.detail:
	clientX - X position of cursor relative to page
	clientY - Y position of cursor relative to page
	pageX - X position of cursor relative to element
	pageY- Y position of cursor relative to element

### dragMove ###

event.detail:
	x: total movement of cursor in x
	y: total movement of cursor in y
	dX: increment in x since last event
	dY: increment in y since last event
	clientX - X position of cursor relative to page
	clientY - Y position of cursor relative to page
	pageX - X position of cursor relative to element
	pageY- Y position of cursor relative to element

### dragStop ###

event.detail:
	x: total movement of cursor in x
	y: total movement of cursor in y
	clientX - X position of cursor relative to page
	clientY - Y position of cursor relative to page
	pageX - X position of cursor relative to element
	pageY- Y position of cursor relative to element

*/

var threshold = 20;
threshold = Math.pow(threshold, 2);

function setupDrag() {
	document.addEventListener('mousedown', function(event) {
		var target = event.target;
		var startX = event.clientX;
		var startY = event.clientY;
		var pageX = event.pageX;
		var pageY = event.pageY;
		var x = startX;
		var y = startY;
		var startListener = function(event) {
			var tx = event.clientX-startX;
			var ty = event.clientY-startY;
			if(Math.pow(tx,2)+Math.pow(ty,2) >= threshold) {
				var ev = document.createEvent('CustomEvent');
				ev.initCustomEvent(
					'dragStart',
					true,
					true,
					{
						clientX: startX,
						clientY: startY,
						pageX: pageX,
						pageY: pageY
					}
				);
				target.dispatchEvent(ev);
				window.removeEventListener('mousemove', startListener);
				window.addEventListener('mousemove', dragListener);
				window.addEventListener('mouseup', stopDispatcher);
			}
		};
		var dragListener = function(event) {
			var ev = document.createEvent('CustomEvent');
			ev.initCustomEvent(
				'dragMove',
				true,
				true,
				{
					x: event.clientX - startX,
					y: event.clientY - startY,
					dX: event.clientX - x,
					dY: event.clientY - y
					clientX: event.clientX,
					clientY: event.clientY,
					pageX: event.clientX + pageX - startX,
					pageY: event.clientY + pageY - startY,
				}
			);
			x = event.clientX;
			y = event.clientY;
			target.dispatchEvent(ev);
		};
		var stopListener = function() {
			window.removeEventListener('mousemove', startListener);
			window.removeEventListener('mousemove', dragListener);
			window.removeEventListener('mouseup', stopListener);
		}
		var stopDispatcher = function(event) {
			var ev = document.createEvent('CustomEvent');
			ev.initCustomEvent(
				'dragStop',
				true,
				true,
				{
					x: event.clientX - startX,
					y: event.clientY - startY,
					clientX: event.clientX,
					clientY: event.clientY,
					pageX: event.clientX + pageX - startX,
					pageY: event.clientY + pageY - startY,
				}
			);
			
			target.dispatchEvent(ev);
			window.removeEventListener('mouseup', stopDispatcher);
		};
		window.addEventListener('mousemove', startListener);
		window.addEventListener('mouseup', stopListener);
		startListener(event);
	});
}
setupDrag();
